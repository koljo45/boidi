﻿using Hr.Fer.MV.Boids.Settings;
using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class AnimalController : BoidController
    {
        protected override Vector3 CalculateHeading(BoidUpdateData updateData)
        {
            Vector3 targetHeading = base.CalculateHeading(updateData);
            AnimalUpdateData animalUpdateData = updateData as AnimalUpdateData;
            AnimalSettings animalSettings = animalUpdateData.Settings as AnimalSettings;
            if (animalUpdateData == null)
                return targetHeading;

            Vector3 predatorAvoidanceForce = Vector3.zero;
            Vector3 preyPursueForce = Vector3.zero;
            if (animalUpdateData.PredatorAvoidanceHeading != Vector3.zero)
            {
                predatorAvoidanceForce = animalUpdateData.PredatorAvoidanceHeading * animalSettings.AvoidPredatorWeight;
            }
            if (animalUpdateData.PreyPursueHeading != Vector3.zero)
            {
                preyPursueForce = animalUpdateData.PreyPursueHeading * animalSettings.PursuePreyWeight;
            }

            return targetHeading + predatorAvoidanceForce + preyPursueForce;
        }
    }
}
