﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public struct AnimalData
    {
        public Vector3 Position;
        public Vector3 Direction;

        public Vector3 FlockHeading;
        public Vector3 FlockCentre;
        public Vector3 NeighbourAvoidanceHeading;
        public int NumFlockmates;

        public Vector3 PredatorAvoidanceHeading;
        public Vector3 PreyPursueHeading;

        public static int Size
        {
            get
            {
                return sizeof(float) * 3 * 7 + sizeof(int);
            }
        }
    }
}