﻿using Hr.Fer.MV.Boids.Settings;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace Hr.Fer.MV.Boids.Classic
{
    public class AnimalManager : MonoBehaviour
    {
        private static HashSet<int> _takenLayers = new HashSet<int>();

        public const int THREAD_GROUP_SIZE = 1024;
        public const int NUM_LAYERS = 1024;

        [SerializeField] private AnimalSettings _settings;
        [SerializeField] private ComputeShader _compute;
        [Range(0, 31)]
        [SerializeField] private int _layer;
        [SerializeField] private bool _searchForAnimals;

        private List<BoidController> _animals;
        private List<BoidController> _predators;
        private List<BoidController> _prey;

        private int _oldLayer = -1;
        private void OnValidate()
        {
            if (_takenLayers.Contains(_layer))
            {
                _layer = -1;
                for (int i = 0; i < NUM_LAYERS; i++)
                    if (!_takenLayers.Contains(i))
                        _layer = i;
            }
            if (_layer == -1)
                Debug.LogError($"Too many instances of {nameof(AnimalManager)}, not enough layers.");

            _takenLayers.Remove(_oldLayer);
            _oldLayer = _layer;
        }

        private void Awake()
        {
        }

        private void Start()
        {
            if (_searchForAnimals)
            {
                _animals = new List<BoidController>();
                foreach (AnimalController ac in FindObjectsOfType<AnimalController>())
                    if (ac.gameObject.layer == _layer)
                        _animals.Add(ac);
            }
            else
            {
                if (Spawner.BoidCache.ContainsKey(_layer))
                    _animals = Spawner.BoidCache[_layer];
                else
                {
                    Debug.LogWarning($"No boids in layer {_layer} for {nameof(AnimalManager)} to use");
                    _animals = new List<BoidController>();
                }
            }
            _predators = Spawner.BoidCache.ContainsKey(_settings.PredatorLayer) ? Spawner.BoidCache[_settings.PredatorLayer] : null;
            _prey = Spawner.BoidCache.ContainsKey(_settings.PreyLayer) ? Spawner.BoidCache[_settings.PreyLayer] : null;
        }

        private void Update()
        {
            if (_animals.Count == 0)
            {
                return;
            }
            updateBoids(_animals);
        }

        private void updateBoids(List<BoidController> animals)
        {
            Profiler.BeginSample("LoadAnimalComputeBuffer");
            int numAnimals = animals.Count;
            var animalData = new AnimalData[numAnimals];

            for (int i = 0; i < animals.Count; i++)
            {
                animalData[i].Position = animals[i].Transform.position;
                animalData[i].Direction = animals[i].Transform.forward;
            }
            var animalBuffer = new ComputeBuffer(numAnimals, AnimalData.Size);
            animalBuffer.SetData(animalData);
            _compute.SetBuffer(0, "animals", animalBuffer);
            Profiler.EndSample();

            int numPrey = 0;
            ComputeBuffer preyBuffer;
            if (_prey != null)
            {
                TransformData[] preyData = new TransformData[_prey.Count];
                for (int i = 0; i < preyData.Length; i++)
                {
                    preyData[i].Position = _prey[i].Transform.position;
                    preyData[i].Direction = _prey[i].Transform.forward;
                }
                preyBuffer = new ComputeBuffer(preyData.Length, TransformData.Size);
                preyBuffer.SetData(preyData);
                numPrey = preyData.Length;
            }
            else
            {
                preyBuffer = new ComputeBuffer(1, TransformData.Size);
            }
            _compute.SetInt("numPrey", numPrey);
            _compute.SetBuffer(0, "prey", preyBuffer);

            int numPredators = 0;
            ComputeBuffer predatorBuffer;
            if (_predators != null)
            {
                TransformData[] predatorData = new TransformData[_predators.Count];
                for (int i = 0; i < predatorData.Length; i++)
                {
                    predatorData[i].Position = _predators[i].Transform.position;
                    predatorData[i].Direction = _predators[i].Transform.forward;
                }
                predatorBuffer = new ComputeBuffer(predatorData.Length, TransformData.Size);
                predatorBuffer.SetData(predatorData);

                numPredators = predatorData.Length;
            }
            else
            {
                predatorBuffer = new ComputeBuffer(1, TransformData.Size);
            }
            _compute.SetInt("numPredators", numPredators);
            _compute.SetBuffer(0, "predators", predatorBuffer);

            _compute.SetInt("numAnimals", animals.Count);
            _compute.SetFloat("viewRadius", _settings.PerceptionRadius);
            _compute.SetFloat("neighbourAvoidanceRadius", _settings.NeighbourAvoidanceRadius);

            int threadGroups = Mathf.CeilToInt(numAnimals / (float)THREAD_GROUP_SIZE);
            _compute.Dispatch(0, threadGroups, 1, 1);

            animalBuffer.GetData(animalData);

            AnimalUpdateData aud = new AnimalUpdateData();
            aud.Settings = _settings;
            Profiler.BeginSample("Updating Boids");
            for (int i = 0; i < animals.Count; i++)
            {
                AnimalController animal = (AnimalController)animals[i];
                aud.AvgFlockHeading = animalData[i].FlockHeading;
                aud.CentreOfFlockmates = animalData[i].FlockCentre;
                aud.NeighbourAvoidanceHeading = animalData[i].NeighbourAvoidanceHeading;
                aud.NumPerceivedFlockmates = animalData[i].NumFlockmates;
                aud.PredatorAvoidanceHeading = animalData[i].PredatorAvoidanceHeading;
                aud.PreyPursueHeading = animalData[i].PreyPursueHeading;
                
                animal.UpdateBoid(aud);
            }
            Profiler.EndSample();

            animalBuffer.Release();
            preyBuffer.Release();
            predatorBuffer.Release();
        }
    }
}