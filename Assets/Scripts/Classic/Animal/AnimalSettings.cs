﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Settings
{
    [CreateAssetMenu(fileName = "AnimalSettings", menuName = "Boids/AnimalSettings", order = 2)]
    public class AnimalSettings : BoidSettings
    {
        [SerializeField] private float _hp = 10;
        [SerializeField] private float _damage = 1;

        [SerializeField] private int _layer;
        [SerializeField] private int _predatorLayer;
        [SerializeField] private int _preyLayer;

        [SerializeField] private float _avoidPredatorWeight = 10;
        [SerializeField] private float _pursuePreyWeight = 5;

        public float Hp
        {
            get
            {
                return _hp;
            }
        }
        public float Damage
        {
            get
            {
                return _damage;
            }
        }
        public int Layer
        {
            get
            {
                return _layer;
            }
        }
        public int PredatorLayer
        {
            get
            {
                return _predatorLayer;
            }
        }
        public int PreyLayer
        {
            get
            {
                return _preyLayer;
            }
        }
        public float AvoidPredatorWeight
        {
            get
            {
                return _avoidPredatorWeight;
            }
        }
        public float PursuePreyWeight
        {
            get
            {
                return _pursuePreyWeight;
            }
        }
    }
}
