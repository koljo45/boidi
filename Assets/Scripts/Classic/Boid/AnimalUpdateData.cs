﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class AnimalUpdateData : BoidUpdateData
    {
        public Vector3 PredatorAvoidanceHeading;
        public Vector3 PreyPursueHeading;
        public int NumPerceivedPredators;
        public int NumPerceivedPrey;
    }
}
