﻿using Hr.Fer.MV.Boids.Settings;
using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class BoidController : MonoBehaviour
    {
        public Transform Transform { get; private set; }

        private void Awake()
        {
            Transform = transform;
        }

        public void SetColour(Color col)
        {
            var material = transform.GetComponentInChildren<MeshRenderer>().material;
            if (material != null)
            {
                material.color = col;
            }
        }

        public void UpdateBoid(BoidUpdateData updateData)
        {
            Vector3 flockHeading = Vector3.Normalize(CalculateHeading(updateData));
            Vector3 collisionAvoidanceHeading = CalculateCollisionAvoidanceHeading();

            var deltaTime = Mathf.Min(0.05f, Time.deltaTime);

            var flockRot = deltaTime * updateData.Settings.MaxSteerAcceleration * (flockHeading - Transform.forward);

            var nextHeading = Vector3.Normalize(Transform.forward + flockRot);

            Vector3 collisionAvoidanceRot = Vector3.zero;
            if (!collisionAvoidanceHeading.Equals(Vector3.zero))
                collisionAvoidanceRot = deltaTime * CollisionSettingsHolder.CollisionSettings.AvoidCollisionWeight * (Vector3.Normalize(collisionAvoidanceHeading) - nextHeading);

            nextHeading = Vector3.Normalize(nextHeading + collisionAvoidanceRot);

            Transform.position += nextHeading * updateData.Settings.Speed * deltaTime;
            Transform.rotation = Quaternion.LookRotation(nextHeading, Vector3.up);
        }

        protected virtual Vector3 CalculateHeading(BoidUpdateData updateData)
        {
            Vector3 heading = Vector3.zero;

            if (updateData.NumPerceivedFlockmates != 0)
            {
                Vector3 alignmentForce = updateData.Settings.AlignWeight * updateData.AvgFlockHeading;
                Vector3 cohesionForce = updateData.Settings.CohesionWeight * Vector3.Normalize(updateData.CentreOfFlockmates - Transform.position);
                Vector3 seperationForce = updateData.Settings.SeperateWeight * updateData.NeighbourAvoidanceHeading;

                heading += alignmentForce + cohesionForce + seperationForce;
            }
            return heading;
        }

        private Vector3 CalculateCollisionAvoidanceHeading()
        {
            Vector3 heading = Vector3.zero;

            if (IsHeadingForCollision())
            {
                Vector3 collisionAvoidDir = BoidHelper.GetClearPath(Transform, CollisionSettingsHolder.CollisionSettings.BoundsRadius, CollisionSettingsHolder.CollisionSettings.CollisionAvoidDst, CollisionSettingsHolder.CollisionSettings.ObstacleMask);
                heading += Vector3.Normalize(collisionAvoidDir);
            }
            else
            {
                Vector3 pos = Transform.position + Transform.forward * CollisionSettingsHolder.CollisionSettings.CollisionAvoidDst;
                Vector4 homoPos = new Vector4(pos.x, pos.y, pos.z, 1);

                for (int i = 0; i < CollisionSettingsHolder.Walls.Length; i++)
                {
                    if (Vector4.Dot(CollisionSettingsHolder.Walls[i], homoPos) < 0)
                    {
                        heading += CollisionSettingsHolder.WallNormals[i];
                    }
                }
            }

            return heading;
        }

        protected bool IsHeadingForCollision()
        {
            return false;
            // Omoguciti za objektne prepreke
            //return Physics.Raycast(Transform.position, Transform.forward, CollisionSettingsHolder.CollisionSettings.CollisionAvoidDst, CollisionSettingsHolder.CollisionSettings.ObstacleMask);
        }
    }
}