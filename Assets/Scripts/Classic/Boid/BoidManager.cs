﻿using Hr.Fer.MV.Boids.Settings;
using System.Collections.Generic;
using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class BoidManager : MonoBehaviour
    {
        public const int THREAD_GROUP_SIZE = 1024;

        [SerializeField] private BoidSettings _settings;
        [SerializeField] private ComputeShader _compute;

        private List<BoidController> _boids;

        void Awake()
        {
        }

        private void Start()
        {
            //_boids = Spawner.boidCache[LayerMask.NameToLayer("Boid")];
            _boids = new List<BoidController>(FindObjectsOfType<BoidController>());
        }

        void FixedUpdate()
        {
            if (_boids.Count == 0)
            {
                return;
            }
            updateBoids(_boids);
        }

        private void updateBoids(List<BoidController> boids)
        {
            int numBoids = boids.Count;
            var boidData = new BoidData[numBoids];

            for (int i = 0; i < boids.Count; i++)
            {
                boidData[i].Position = boids[i].transform.position;
                boidData[i].Direction = boids[i].transform.forward;
            }

            var boidBuffer = new ComputeBuffer(numBoids, BoidData.Size);
            boidBuffer.SetData(boidData);

            _compute.SetBuffer(0, "boids", boidBuffer);
            _compute.SetInt("numBoids", boids.Count);
            _compute.SetFloat("viewRadius", _settings.PerceptionRadius);
            _compute.SetFloat("neighbourAvoidanceRadius", _settings.NeighbourAvoidanceRadius);

            int threadGroups = Mathf.CeilToInt(numBoids / (float)THREAD_GROUP_SIZE);
            _compute.Dispatch(0, threadGroups, 1, 1);

            boidBuffer.GetData(boidData);

            BoidUpdateData bud = new BoidUpdateData();
            bud.Settings = _settings;
            for (int i = 0; i < boids.Count; i++)
            {
                bud.AvgFlockHeading = boidData[i].FlockHeading;
                bud.CentreOfFlockmates = boidData[i].FlockCentre;
                bud.NeighbourAvoidanceHeading = boidData[i].NeighbourAvoidanceHeading;
                bud.NumPerceivedFlockmates = boidData[i].NumFlockmates;

                boids[i].UpdateBoid(bud);
            }

            boidBuffer.Release();
        }
    }
}