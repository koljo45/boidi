﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Settings
{
    [CreateAssetMenu(fileName = "BoidSettings", menuName = "Boids/BoidSettings", order = 1)]
    public class BoidSettings : ScriptableObject
    {
        // Settings
        [SerializeField] private float _speed = 2;
        [SerializeField] private float _perceptionRadius = 2.5f;
        [SerializeField] private float _neighbourAvoidanceRadius = 1;
        [SerializeField] private float _maxSteerAcceleration = 3;

        [SerializeField] private float _alignWeight = 1;
        [SerializeField] private float _cohesionWeight = 1;
        [SerializeField] private float _seperateWeight = 1;

        public float Speed
        {
            get
            {
                return _speed;
            }
        }
        public float PerceptionRadius
        {
            get
            {
                return _perceptionRadius;
            }
        }
        public float NeighbourAvoidanceRadius
        {
            get
            {
                return _neighbourAvoidanceRadius;
            }
        }
        public float MaxSteerAcceleration
        {
            get
            {
                return _maxSteerAcceleration;
            }
        }

        public float AlignWeight
        {
            get
            {
                return _alignWeight;
            }
        }
        public float CohesionWeight
        {
            get
            {
                return _cohesionWeight;
            }
        }
        public float SeperateWeight
        {
            get
            {
                return _seperateWeight;
            }
        }
    }
}