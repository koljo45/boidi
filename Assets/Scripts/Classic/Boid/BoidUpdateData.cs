﻿using Hr.Fer.MV.Boids.Settings;
using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class BoidUpdateData
    {
        public BoidSettings Settings;

        public Vector3 AvgFlockHeading;
        public Vector3 NeighbourAvoidanceHeading;
        public Vector3 CentreOfFlockmates;
        public int NumPerceivedFlockmates;
    }
}
