﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public static class BoidHelper
    {
        private const int _numViewDirections = 300;
        public static readonly Vector3[] Directions;

        static BoidHelper()
        {
            Directions = new Vector3[_numViewDirections];

            float goldenRatio = (1 + Mathf.Sqrt(5)) / 2;
            float angleIncrement = Mathf.PI * 2 * goldenRatio;

            for (int i = 0; i < _numViewDirections; i++)
            {
                float t = (float)i / _numViewDirections;
                float inclination = Mathf.Acos(1 - 2 * t);
                float azimuth = angleIncrement * i;

                float x = Mathf.Sin(inclination) * Mathf.Cos(azimuth);
                float y = Mathf.Sin(inclination) * Mathf.Sin(azimuth);
                float z = Mathf.Cos(inclination);
                Directions[i] = new Vector3(x, y, z);
            }
        }

        public static Vector3 GetClearPath(Transform trans, float rayRadius, float rayMaxDistance, LayerMask obstacleMask)
        {
            for (int i = 0; i < Directions.Length; i++)
            {
                Vector3 dir = trans.TransformDirection(Directions[i]);
                Ray ray = new Ray(trans.position, dir);

                if (!Physics.Raycast(ray, rayMaxDistance, obstacleMask) && !Physics.SphereCast(ray, rayRadius, rayMaxDistance, obstacleMask))
                {
                    return dir;
                }
            }

            return trans.forward;
        }

    }
}