﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Settings
{
    [CreateAssetMenu(fileName = "CollisionSettings", menuName = "Boids/CollisionSettings", order = 3)]
    public class CollisionSettings : ScriptableObject
    {
        [SerializeField] private LayerMask _obstacleMask;
        [SerializeField] private float _boundsRadius = .27f;
        [SerializeField] private float _avoidCollisionWeight = 10;
        [SerializeField] private float _collisionAvoidDst = 5;

        public LayerMask ObstacleMask
        {
            get
            {
                return _obstacleMask;
            }
        }
        public float BoundsRadius
        {
            get
            {
                return _boundsRadius;
            }
        }
        public float AvoidCollisionWeight
        {
            get
            {
                return _avoidCollisionWeight;
            }
        }
        public float CollisionAvoidDst
        {
            get
            {
                return _collisionAvoidDst;
            }
        }
    }
}