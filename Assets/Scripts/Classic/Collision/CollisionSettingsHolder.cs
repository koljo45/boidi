﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Settings
{
    public static class CollisionSettingsHolder
    {
        public static CollisionSettings CollisionSettings;

        public static Vector4[] Walls;

        public static Vector3[] WallNormals;
    }
}
