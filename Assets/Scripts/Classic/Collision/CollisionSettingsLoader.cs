﻿using Hr.Fer.MV.Boids.Settings;
using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class CollisionSettingsLoader : MonoBehaviour
    {
        [SerializeField]
        private CollisionSettings _collisionSettings;

        [SerializeField]
        private Transform[] _walls;
        private void Awake()
        {
            CollisionSettingsHolder.CollisionSettings = _collisionSettings;

            CollisionSettingsHolder.Walls = new Vector4[_walls.Length];
            CollisionSettingsHolder.WallNormals = new Vector3[_walls.Length];

            for (int i = 0; i < _walls.Length; i++)
            {
                Vector3 fow = _walls[i].forward;
                CollisionSettingsHolder.Walls[i] = new Vector4(fow.x, fow.y, fow.z, -Vector3.Dot(fow, _walls[i].position));
                CollisionSettingsHolder.WallNormals[i] = fow;
            }
        }
    }
}
