﻿using System.Collections.Generic;
using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public class Spawner : MonoBehaviour
    {
        public static Dictionary<int, List<BoidController>> BoidCache = new Dictionary<int, List<BoidController>>();

        public enum GizmoType { Never, SelectedOnly, Always }

        [SerializeField] private BoidController _boidPrefab;
        [SerializeField] private int _count;

        [SerializeField] private float _spawnRadius = 10;
        [SerializeField] private GizmoType _showSpawnRegion;
        [SerializeField] private Color _colour;

        private void Awake()
        {
            int layer = _boidPrefab.gameObject.layer;

            if (!BoidCache.ContainsKey(layer))
            {
                BoidCache.Add(layer, new List<BoidController>());
            }
            List<BoidController> layerBoids = BoidCache[layer];

            for (int j = 0; j < _count; j++)
            {
                Vector3 dir = Random.insideUnitSphere;
                Vector3 pos = transform.position + dir * _spawnRadius;
                BoidController boid = Instantiate(_boidPrefab);
                boid.transform.position = pos;
                boid.transform.forward = dir;

                boid.SetColour(_colour);

                layerBoids.Add(boid);
            }
        }

        private void OnDrawGizmos()
        {
            if (_showSpawnRegion == GizmoType.Always)
            {
                DrawGizmos();
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (_showSpawnRegion == GizmoType.SelectedOnly)
            {
                DrawGizmos();
            }
        }

        private void DrawGizmos()
        {
            Gizmos.color = new Color(_colour.r, _colour.g, _colour.b, 0.3f);
            Gizmos.DrawSphere(transform.position, _spawnRadius);
        }

    }
}