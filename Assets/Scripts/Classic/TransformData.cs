﻿using UnityEngine;

namespace Hr.Fer.MV.Boids.Classic
{
    public struct TransformData
    {
        public Vector3 Position;
        public Vector3 Direction;

        public static int Size
        {
            get
            {
                return sizeof(float) * 3 * 2;
            }
        }
    }
}