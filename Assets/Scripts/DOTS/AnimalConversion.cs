#if UNITY_EDITOR

using Unity.Entities;
using Unity.Transforms;

namespace Hr.Fer.MV.Boids.DOTS
{
    [UpdateInGroup(typeof(GameObjectConversionGroup))]
    [ConverterVersion("mvidekovic", 1)]
    public class AnimalConversion : GameObjectConversionSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((AnimalAuthoring animalAuthoring) =>
            {
                var entity = GetPrimaryEntity(animalAuthoring);

                DstEntityManager.AddSharedComponentData(entity, new Animal
                {
                    Speed = animalAuthoring.Settings.Speed,
                    PerceptionRadius = animalAuthoring.Settings.PerceptionRadius,
                    NeighbourAvoidanceRadius = animalAuthoring.Settings.NeighbourAvoidanceRadius,
                    MaxSteerAcceleration = animalAuthoring.Settings.MaxSteerAcceleration,
                    AlignWeight = animalAuthoring.Settings.AlignWeight,
                    CohesionWeight = animalAuthoring.Settings.CohesionWeight,
                    SeperateWeight = animalAuthoring.Settings.SeperateWeight,
                    PredatorLayer = animalAuthoring.Settings.PredatorLayer,
                    PreyLayer = animalAuthoring.Settings.PreyLayer,
                    AvoidPredatorWeight = animalAuthoring.Settings.AvoidPredatorWeight,
                    PursuePreyWeight = animalAuthoring.Settings.PursuePreyWeight
                });
                DstEntityManager.AddSharedComponentData(entity, new Layer { Index = animalAuthoring.Settings.Layer });

                DstEntityManager.RemoveComponent<Translation>(entity);
                DstEntityManager.RemoveComponent<Rotation>(entity);
            });
        }
    }
}

#endif
