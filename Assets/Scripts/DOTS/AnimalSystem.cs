﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Transforms;

// Mike's GDC Talk on 'A Data Oriented Approach to Using Component Systems'
// is a great reference for dissecting the Boids sample code:
// https://youtu.be/p65Yt20pw0g?t=1446
// It explains a slightly older implementation of this sample but almost all the
// information is still relevant.

namespace Hr.Fer.MV.Boids.DOTS
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public class AnimalSystem : SystemBase
    {
        EntityQuery _boidQuery;
        EntityQuery _layerQuery;

        // In this sample there are 3 total unique boid variants, one for each unique value of the 
        // Boid SharedComponent (note: this includes the default uninitialized value at
        // index 0, which isnt actually used in the sample).
        List<Animal> _uniqueBoids = new List<Animal>(3);
        List<Layer> _allLayers = new List<Layer>(3);

        [BurstCompile]
        struct LoadLayerData : IJobParallelFor
        {
            [ReadOnly] public NativeArray<LocalToWorld> EntityData;

            public NativeArray<float3> Positions;

            public void Execute(int index)
            {
                Positions[index] = EntityData[index].Position;
            }
        }

        [BurstCompile]
        struct LoadFlockData : IJobParallelFor
        {
            [ReadOnly] public NativeArray<LocalToWorld> EntityData;

            public NativeArray<float3> Positions;
            public NativeArray<float3> Headings;

            public void Execute(int index)
            {
                Positions[index] = EntityData[index].Position;
                Headings[index] = EntityData[index].Forward;
            }
        }

        private Dictionary<int, NativeArray<float3>> layersData = new Dictionary<int, NativeArray<float3>>();
        private Layer defaultLayer;
        protected override void OnUpdate()
        {
            EntityManager.GetAllUniqueSharedComponentData(_allLayers);
            EntityManager.GetAllUniqueSharedComponentData(_uniqueBoids);
            layersData.Clear();

            var copyLayerPositionsJobHandle = Dependency;
            for (int i = 0; i < _allLayers.Count; i++)
            {
                _layerQuery.SetSharedComponentFilter(_allLayers[i]);
                if (_layerQuery.CalculateEntityCount() == 0)
                {
                    _layerQuery.ResetFilter();
                    continue;
                }
                defaultLayer = _allLayers[i];
                var temp = layersData[_allLayers[i].Index] = new NativeArray<float3>(_layerQuery.CalculateEntityCount(), Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                _layerQuery.ResetFilter();

                var handle = Entities
                    .WithName("CopyLayersPositionsJob")
                    .WithSharedComponentFilter(_allLayers[i])
                    .ForEach((int entityInQueryIndex, in LocalToWorld localToWorld) =>
                    {
                        temp[entityInQueryIndex] = localToWorld.Position;
                    })
                    .ScheduleParallel(Dependency);

                copyLayerPositionsJobHandle = JobHandle.CombineDependencies(copyLayerPositionsJobHandle, handle);
            }

            float deltaTime = math.min(0.05f, Time.DeltaTime);

            JobHandle steerJobHandle = Dependency;
            for (int i = 0; i < _uniqueBoids.Count; i++)
            {
                Animal animalSettings = _uniqueBoids[i];

                _boidQuery.AddSharedComponentFilter(animalSettings);
                int chunkCount = _boidQuery.CalculateEntityCount();
                if (chunkCount == 0)
                {
                    _boidQuery.ResetFilter();
                    continue;
                }

                // Ucitavanje predatora i lovine
                NativeArray<float3> predatorLayer;
                NativeArray<float3> preyLayer;
                bool hasPredators = layersData.TryGetValue(animalSettings.PredatorLayer, out predatorLayer);
                bool hasPrey = layersData.TryGetValue(animalSettings.PreyLayer, out preyLayer);
                predatorLayer = hasPredators ? predatorLayer : layersData[defaultLayer.Index];
                preyLayer = hasPrey ? preyLayer : layersData[defaultLayer.Index];

                NativeArray<float3> positions = new NativeArray<float3>(chunkCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                NativeArray<float3> headings = new NativeArray<float3>(chunkCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

                JobHandle loadAnimalDataJobHandle =
                    Entities
                    .WithName("LoadAnimalData")
                    .WithSharedComponentFilter(animalSettings)
                    .ForEach((int entityInQueryIndex, in LocalToWorld localToWorld) =>
                    {
                        positions[entityInQueryIndex] = localToWorld.Position;
                        headings[entityInQueryIndex] = localToWorld.Forward;
                    }).ScheduleParallel(Dependency);

                var steeringJobDependency = JobHandle.CombineDependencies(loadAnimalDataJobHandle, copyLayerPositionsJobHandle);

                var jobHandle = Entities
                    .WithName("Steer")
                    .WithSharedComponentFilter(animalSettings)
                    .WithReadOnly(positions)
                    .WithReadOnly(headings)
                    .WithReadOnly(predatorLayer)
                    .WithReadOnly(preyLayer)
                    .ForEach((int entityInQueryIndex, ref LocalToWorld localToWorld) =>
                    {
                        float perceptionSquared = animalSettings.PerceptionRadius * animalSettings.PerceptionRadius;
                        float avoidSquared = animalSettings.NeighbourAvoidanceRadius * animalSettings.NeighbourAvoidanceRadius;
                        float3 position = positions[entityInQueryIndex];
                        float3 forward = headings[entityInQueryIndex];
                        int numNeighbours = 0;
                        float3 center = new float3();
                        float3 alignment = new float3();
                        float3 separation = new float3(0, 0, 0);

                        for (int p = 0; p < positions.Length; p++)
                        {
                            float3 offset = positions[p] - position;
                            float lenSq = math.lengthsq(offset);
                            if (lenSq < perceptionSquared)
                            {
                                center += positions[p];
                                alignment += headings[p];
                                numNeighbours++;
                                if (lenSq < avoidSquared)
                                {
                                    if (p == entityInQueryIndex)
                                        continue;
                                    separation -= offset / lenSq;
                                }
                            }
                        }
                        // Brze nego svaku iteraciju provjeravati p == entityInQueryIndex
                        center -= position;
                        alignment -= forward;
                        numNeighbours--;

                        float3 predatorHeading = new float3(0, 0, 0);
                        if (hasPredators)
                            for (int b = 0; b < predatorLayer.Length; b++)
                            {
                                float3 offset = predatorLayer[b] - position;
                                float lenSq = math.lengthsq(offset);
                                if (lenSq < perceptionSquared)
                                {
                                    predatorHeading -= offset / lenSq;
                                }
                            }

                        float3 preyHeading = new float3(0, 0, 0);
                        if (hasPrey)
                            for (int b = 0; b < preyLayer.Length; b++)
                            {
                                float3 offset = preyLayer[b] - position;
                                float lenSq = math.lengthsq(offset);
                                if (lenSq < perceptionSquared)
                                {
                                    preyHeading += offset / lenSq;
                                }
                            }

                        var alignmentForce = animalSettings.AlignWeight
                                                              * math.normalizesafe(alignment / numNeighbours, float3.zero);

                        var cohesionForce = animalSettings.CohesionWeight
                                                      * math.normalizesafe(center - (position * numNeighbours), float3.zero);

                        var seperationForce = animalSettings.SeperateWeight
                                                      * math.normalizesafe(separation, float3.zero);

                        var predatorAvoidanceForce = animalSettings.AvoidPredatorWeight
                                                      * math.normalizesafe(predatorHeading, float3.zero);

                        var preyPursueForce = animalSettings.PursuePreyWeight * math.normalizesafe(preyHeading, float3.zero);

                        var targetHeading = math.normalizesafe(alignmentForce + cohesionForce + seperationForce + predatorAvoidanceForce + preyPursueForce, float3.zero);

                        var nextHeading = math.normalizesafe(forward + deltaTime * animalSettings.MaxSteerAcceleration * (targetHeading - forward), float3.zero);
                        localToWorld = new LocalToWorld
                        {
                            Value = float4x4.TRS(
                                position + (nextHeading * animalSettings.Speed * deltaTime),
                                quaternion.LookRotationSafe(nextHeading, math.up()),
                                new float3(1.0f, 1.0f, 1.0f))
                        };
                    })
                    .ScheduleParallel(steeringJobDependency);

                positions.Dispose(jobHandle);
                headings.Dispose(jobHandle);

                steerJobHandle = JobHandle.CombineDependencies(steerJobHandle, jobHandle);

                Dependency = steerJobHandle;
                //_boidQuery.AddDependency(steerJobHandle);
                _boidQuery.ResetFilter();
            }

            var disposeLayersData = steerJobHandle;
            foreach (var data in layersData.Values)
                disposeLayersData = JobHandle.CombineDependencies(disposeLayersData, data.Dispose(steerJobHandle));

            Dependency = disposeLayersData;

            _boidQuery.AddDependency(Dependency);
            _uniqueBoids.Clear();
            _allLayers.Clear();
        }

        protected override void OnCreate()
        {
            _boidQuery = GetEntityQuery(new EntityQueryDesc
            {
                All = new[] { ComponentType.ReadOnly<Animal>(), ComponentType.ReadOnly<Layer>(), ComponentType.ReadWrite<LocalToWorld>() },
            });

            _layerQuery = GetEntityQuery(ComponentType.ReadOnly<Layer>(), ComponentType.ReadOnly<LocalToWorld>());

            RequireForUpdate(_boidQuery);
        }
    }
}