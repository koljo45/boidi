using System;
using Unity.Entities;
using UnityEngine;
using Unity.Transforms;

namespace Hr.Fer.MV.Boids.DOTS
{
    [Serializable]
    [WriteGroup(typeof(LocalToWorld))]
    public struct Boid : ISharedComponentData
    {
        public float Speed;
        public float PerceptionRadius;
        public float NeighbourAvoidanceRadius;
        public float MaxSteerAcceleration;

        public float AlignWeight;
        public float CohesionWeight;
        public float SeperateWeight;

        public float BoundsRadius;
        public float AvoidCollisionWeight;
        public float CollisionAvoidDst;
    }
}
