#if UNITY_EDITOR

using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

namespace Hr.Fer.MV.Boids.DOTS
{
    [UpdateInGroup(typeof(GameObjectConversionGroup))]
    [ConverterVersion("mvidekovic", 1)]
    public class BoidConversion : GameObjectConversionSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((BoidAuthoring boidAuthoring) =>
            {
                var entity = GetPrimaryEntity(boidAuthoring);

                DstEntityManager.AddSharedComponentData(entity, new Boid
                {
                    Speed = boidAuthoring.Settings.Speed,
                    PerceptionRadius = boidAuthoring.Settings.PerceptionRadius,
                    NeighbourAvoidanceRadius = boidAuthoring.Settings.NeighbourAvoidanceRadius,
                    MaxSteerAcceleration = boidAuthoring.Settings.MaxSteerAcceleration,
                    AlignWeight = boidAuthoring.Settings.AlignWeight,
                    CohesionWeight = boidAuthoring.Settings.CohesionWeight,
                    SeperateWeight = boidAuthoring.Settings.SeperateWeight
                });

                DstEntityManager.RemoveComponent<Translation>(entity);
                DstEntityManager.RemoveComponent<Rotation>(entity);
            });
        }
    }
}

#endif
