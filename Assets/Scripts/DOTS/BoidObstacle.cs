using Unity.Entities;
using Unity.Mathematics;

namespace Hr.Fer.MV.Boids.DOTS
{
    public struct BoidObstacle : IComponentData
    {
        public float4 CollisionPlane;
        public float3 PlaneNormal;
    }
}
