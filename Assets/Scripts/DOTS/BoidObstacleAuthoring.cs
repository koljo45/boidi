#if UNITY_EDITOR

using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using UnityEngine;

namespace Hr.Fer.MV.Boids.DOTS
{
    [RequiresEntityConversion]
    [ConverterVersion("mvidekovic", 1)]
    public class BoidObstacleAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var low = dstManager.GetComponentData<LocalToWorld>(entity);

            dstManager.AddComponentData(entity, new BoidObstacle
            {
                CollisionPlane = new float4(low.Forward, -math.dot(low.Forward, low.Position)),
                PlaneNormal = low.Forward
            });
        }
    }
}

#endif
