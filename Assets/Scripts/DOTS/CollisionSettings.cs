﻿using System;
using Unity.Entities;

namespace Hr.Fer.MV.Boids.DOTS
{
    public struct CollisionSettings : IComponentData
    {
        public float AvoidCollisionWeight;
        public float CollisionAvoidDst;
    }
}
