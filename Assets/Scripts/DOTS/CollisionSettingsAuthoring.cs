﻿#if UNITY_EDITOR

using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Hr.Fer.MV.Boids.DOTS
{
    [RequiresEntityConversion]
    [ConverterVersion("mvidekovic", 1)]
    public class CollisionSettingsAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        private static bool _collisionSettingsLoaded = false;

        [SerializeField]
        private Settings.CollisionSettings _collisionSettings;
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            if (_collisionSettingsLoaded)
            {
                Debug.LogWarning($"You should only have one {nameof(CollisionSettingsAuthoring)} component in scene");
                return;
            }

            dstManager.AddComponentData(entity, new CollisionSettings
            {
                CollisionAvoidDst = _collisionSettings.CollisionAvoidDst,
                AvoidCollisionWeight = _collisionSettings.AvoidCollisionWeight
            });
            dstManager.RemoveComponent<LocalToWorld>(entity);
            dstManager.RemoveComponent<Transform>(entity);
            dstManager.RemoveComponent<Rotation>(entity);

            _collisionSettingsLoaded = true;
        }
    }
}

#endif