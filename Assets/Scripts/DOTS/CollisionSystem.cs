﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Hr.Fer.MV.Boids.DOTS
{
    [UpdateAfter(typeof(AnimalSystem))]
    public class CollisionSystem : SystemBase
    {
        private EntityQuery _obstacleQuery;
        private EntityQuery _collisionSettingsQuery;
        private float _collisionAvoidDst = 10;
        private float _avoidCollisionWeight = 50;

        protected override void OnUpdate()
        {
            int numObstacles = _obstacleQuery.CalculateEntityCount();
            if (numObstacles == 0)
                return;

            float collisionAvoidDst = _collisionAvoidDst;
            float avoidCollisionWeight = _avoidCollisionWeight;
            Entities
                .WithStoreEntityQueryInField(ref _collisionSettingsQuery)
                //.WithChangeFilter<CollisionSettings>()
                .WithBurst()
                .ForEach((in CollisionSettings collisionSettings) =>
                {
                    collisionAvoidDst = collisionSettings.CollisionAvoidDst;
                    avoidCollisionWeight = collisionSettings.AvoidCollisionWeight;
                }).Run();

            NativeArray<BoidObstacle> obstacles = new NativeArray<BoidObstacle>(numObstacles, Allocator.TempJob);

            var loadObstaclesJobHandle = Entities
                .WithName("LoadObstacles")
                .ForEach((int entityInQueryIndex, in BoidObstacle obstacle) =>
                {
                    obstacles[entityInQueryIndex] = obstacle;
                }).ScheduleParallel(Dependency);

            var deltaTime = Time.DeltaTime;

            var collisionJobHandle = Entities
                .WithName("BoidCollision")
                .WithAll<Animal>()
                .WithReadOnly(obstacles)
                .ForEach((ref LocalToWorld localToWorld) =>
                {
                    var forward = localToWorld.Forward;
                    var position = localToWorld.Position;
                    var targetPos = new float4(position + localToWorld.Forward * collisionAvoidDst, 1);
                    var obstacleAvoidanceHeading = float3.zero;

                    for (int i = 0; i < obstacles.Length; i++)
                    {         
                        var obstacle = obstacles[i];

                        if (math.dot(targetPos, obstacles[i].CollisionPlane) < 0)
                        {
                            obstacleAvoidanceHeading += obstacles[i].PlaneNormal;
                        }
                    }
                    obstacleAvoidanceHeading = math.normalizesafe(obstacleAvoidanceHeading, float3.zero);
                    if (obstacleAvoidanceHeading.Equals(float3.zero))
                        return;

                    var newRot = quaternion.LookRotationSafe(forward + (obstacleAvoidanceHeading - forward) * deltaTime * avoidCollisionWeight, math.up());

                    localToWorld = new LocalToWorld
                    {
                        Value = float4x4.TRS(position, newRot,
                        new float3(1.0f, 1.0f, 1.0f))
                    };
                }).ScheduleParallel(loadObstaclesJobHandle);

            obstacles.Dispose(collisionJobHandle);

            Dependency = collisionJobHandle;
        }

        protected override void OnCreate()
        {
            _obstacleQuery = GetEntityQuery(ComponentType.ReadOnly<BoidObstacle>());

            RequireForUpdate(_obstacleQuery);
            RequireForUpdate(_collisionSettingsQuery);
        }
    }
}
