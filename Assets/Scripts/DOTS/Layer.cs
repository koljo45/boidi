﻿using Unity.Entities;

namespace Hr.Fer.MV.Boids.DOTS
{
    public struct Layer : ISharedComponentData
    {
        public int Index;
    }
}
