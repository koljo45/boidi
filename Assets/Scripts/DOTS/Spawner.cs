using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine.Profiling;

namespace Hr.Fer.MV.Boids.DOTS
{
    public struct Spawner : IComponentData
    {
        public Entity BoidPrefab;
        public float SpawnRadius;
        public int Count;
    }

    public class SpawnSystem : SystemBase
    {
        [BurstCompile]
        struct SetBoidLocalToWorld : IJobParallelFor
        {
            [NativeDisableContainerSafetyRestriction]
            [NativeDisableParallelForRestriction]
            public ComponentDataFromEntity<LocalToWorld> LocalToWorldFromEntity;
            
            public NativeArray<Entity> Entities;
            public float3 Center;
            public float Radius;
            
            public void Execute(int i)
            {
                var entity = Entities[i];
                var random = new Random(((uint)(entity.Index + i + 1) * 0x9F6ABC1));
                var dir = math.normalizesafe(random.NextFloat3() - new float3(0.5f,0.5f,0.5f));
                var pos = Center + (dir * Radius * random.NextFloat());
                var localToWorld = new LocalToWorld
                {
                  Value = float4x4.TRS(pos, quaternion.LookRotationSafe(dir, math.up()), new float3(1.0f, 1.0f, 1.0f))
                };
                LocalToWorldFromEntity[entity] = localToWorld;
            }
        }

        protected override void OnUpdate()
        {
            Entities.WithStructuralChanges().ForEach((Entity entity, int entityInQueryIndex, in Spawner spawner, in LocalToWorld spawnerLocalToWorld) =>
            {
                var boidEntities = new NativeArray<Entity>(spawner.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                
                Profiler.BeginSample("Instantiate");
                EntityManager.Instantiate(spawner.BoidPrefab, boidEntities);
                Profiler.EndSample();
                
                var localToWorldFromEntity = GetComponentDataFromEntity<LocalToWorld>();
                var setBoidLocalToWorldJob = new SetBoidLocalToWorld
                {
                    LocalToWorldFromEntity = localToWorldFromEntity,
                    Entities = boidEntities,
                    Center = spawnerLocalToWorld.Position,
                    Radius = spawner.SpawnRadius
                };
                Dependency = setBoidLocalToWorldJob.Schedule(spawner.Count, 64, Dependency);
                Dependency = boidEntities.Dispose(Dependency);
                
                EntityManager.DestroyEntity(entity);
            }).Run();
        }
    }
}
