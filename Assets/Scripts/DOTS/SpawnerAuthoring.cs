﻿#if UNITY_EDITOR

using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Hr.Fer.MV.Boids.DOTS
{
    [RequiresEntityConversion]
    [ConverterVersion("mvidekovic", 1)]
    public class SpawnerAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
    {
        public GameObject Prefab;
        public float SpawnRadius;
        public int Count;
        
        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.Add(Prefab);
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new Spawner
            {
                BoidPrefab = conversionSystem.GetPrimaryEntity(Prefab),
                Count = Count,
                SpawnRadius = SpawnRadius
            });
        }
    }
}

#endif
