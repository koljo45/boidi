﻿using System;
using UnityEngine;

namespace Hr.Fer.MV.Boids
{
    public class FPSDisplay : MonoBehaviour
    {
        [SerializeField] private int _numSamples;
        [SerializeField] private float _warmupTime;

        private float[] _samples;
        private int _currentSample;
        private bool _resultPrinted = false;

        private void Awake()
        {
            _samples = new float[_numSamples];
        }

        private void Update()
        {
            if (_warmupTime > 0)
            {
                _warmupTime -= Time.unscaledDeltaTime;
                return;
            }

            if (_currentSample < _numSamples)
            {
                _samples[_currentSample++] = Time.unscaledDeltaTime;
            }
            else if (!_resultPrinted)
            {
                _resultPrinted = true;

                double average = 0;
                for (int i = 0; i < _numSamples; i++)
                    average += _samples[i];
                average /= _numSamples;

                Debug.Log("Average FPS: " + 1 / average);

                double dev = 0;
                for (int i = 0; i < _numSamples; i++)
                {
                    double delta = _samples[i] - average;
                    dev += delta * delta;
                }
                dev = Math.Sqrt(dev / _numSamples);

                Debug.Log("FPS Deviation: " + (1 / average - 1 / (average + dev)));

                System.IO.File.WriteAllText(@"D:\FPS-data.txt", $"Average FPS: {1 / average}\nFPS Deviation: {(1 / average - 1 / (average + dev))}");
            }
        }
    }
}
